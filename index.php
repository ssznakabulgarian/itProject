<?php
    include "commonStuff.php";
    if(isset($_COOKIE['rememberMeToken'])) {
        $stmt_checkToken = $con->prepare("SELECT user_id FROM tokens WHERE token=?") or die ("Failed to prepare the 1st statement!");
        $stmt_checkToken->bind_param('s', $_COOKIE['rememberMeToken']);
        $stmt_checkToken->execute();
        $stmt_checkToken->store_result();
        $stmt_checkToken->bind_result($user_id);
        echo $stmt_checkToken->error;
        if($stmt_checkToken->fetch()){
            $stmt_checkToken->close();
            $stmt_login = $con->prepare("SELECT name FROM users WHERE user_id=?") or die ("Failed to prepare the 2nd statement!");
            $stmt_login->bind_param('i', $user_id);
            $stmt_login->execute();
            $stmt_login->store_result();
            $stmt_login->bind_result($name);
            $stmt_login->fetch();
            
            $hiMessage = "Hi ".$name.", your id is ".$user_id." and you're loged in.";
            //echo $hiMessage;
            $_SESSION["logged-in"] = true;
            $_SESSION["name"] = $name;
            $_SESSION["user_id"] = $user_id;
            
            echo $stmt_login->error;
            $stmt_login->close();
            //refresh token
            //---------------------------------I guess
            $cookie_name = "rememberMeToken";
            $cookie_value_token = $_COOKIE['rememberMeToken'];
            $expDays = 30;
            setcookie($cookie_name, $cookie_value_token, time() + (86400 * $expDays), "/"); // 86400 = 1 day
            //some loged-in message perhaps
            //--------------------------------
        }else{
            //wrong token, perhaps we ain't gonna do anything
        }
    }else {
        echo "no token cookie!";
        //no cookie - can't work with an empty stomach
    }
    //redirect

    header("Location: /index.html");
    //exit();
