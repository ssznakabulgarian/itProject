<?php
    include "commonStuff.php";
    $incomingData = json_decode(file_get_contents("php://input"), true);
    $result = array();

    if (!filter_var($incomingData['email'], FILTER_VALIDATE_EMAIL)) {
        $result["incorrectEmail"] = true;
        $result["success"] = false;
    }else{
        $result["incorrectEmail"] = false;
        $stmt_login = $con->prepare("SELECT user_id, name FROM users WHERE email=? AND password=?") or die ("Failed to prepare the 1st statement!");
        $stmt_login->bind_param('ss', $incomingData['email'], $incomingData['password']);
        $stmt_login->execute();
        echo $stmt_login->error;
        $stmt_login->bind_result($user_id, $name);
        if($stmt_login->fetch()){
            $result["success"] = true;
            $result["user_id"] = $user_id;
            $result["name"] = $name;
        }else{
            $result["success"] = false;
        }
    }

    echo json_encode($result);
    $stmt_login->close();
    //token stuff below

    if($result['success'] && $incomingData['rememberMe'] && !isset($_COOKIE['rememberMeToken'])){
        $cookie_name = "rememberMeToken";
        $cookie_value_token = getToken(64);
        $expDays = 30;
        setcookie($cookie_name, $cookie_value_token, time() + (86400 * $expDays), "/"); // 86400 = 1 day
        
        $stmt_saveToken = $con->prepare("INSERT INTO tokens (user_id, token) VALUES (?, ?)") or die ("Failed to prepare the 2nd statement!");
        $stmt_saveToken->bind_param('ss', $result['user_id'], $cookie_value_token);
        $stmt_saveToken->execute();
        echo $stmt_saveToken->error;
    }
//to be coded: logout (only the current machine's token)
