<?php
    include "commonStuff.php";
    $incomingData = json_decode(file_get_contents("php://input"), true);
    $result = array();

    if (!filter_var($incomingData['email'], FILTER_VALIDATE_EMAIL)) {
        $result["incorrectEmail"] = true;
    }else{
        $result["incorrectEmail"] = false;
    }
    
    $stmt_check = $con->prepare("SELECT * FROM users WHERE email=?") or die ("Failed to prepare the 1st statement!");
    $stmt_check->bind_param('s', $incomingData['email']);
    $stmt_check->execute();
    if($stmt_check->fetch()){
        $result["emailTaken"]=true;
    }else{
        $result["emailTaken"]=false;
    }

    $stmt_check->close();

    $stmt_check = $con->prepare("SELECT * FROM users WHERE name=?") or die ("Failed to prepare the 2nd statement!");
    $stmt_check->bind_param('s', $incomingData['name']);
    $stmt_check->execute();
    if($stmt_check->fetch()){
        $result["nameTaken"]=true;
    }else{
        $result["nameTaken"]=false;
    }

    echo $stmt_check->error;
    
    if($result["emailTaken"] || $result["nameTaken"] || $result["incorrectEmail"]){
        $result["success"]=false;
    }else{
        $stmt_register = $con->prepare("INSERT INTO users (email, name, password) VALUES (?, ?, ?)") or die ("Failed to prepare the 3rd statement!");
        $stmt_register->bind_param('sss', $incomingData['email'], $incomingData['name'], $incomingData['password']);
        $stmt_register->execute();
        $result["success"]=true;
        
        echo $stmt_register->error;
    }
    echo json_encode($result);
